<?php

namespace Safebits\Connector\Exceptions;

/**
 * Class SBConnectorException
 * @package Safebits\Connector
 */
class SBConnectorException extends \Exception
{
    /**
     * ConnectorKnownException constructor.
     * @param $message
     * @param $code
     * @param null $previous
     */
    public function __construct($message, $code, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
