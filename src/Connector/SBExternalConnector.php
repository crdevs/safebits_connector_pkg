<?php

namespace Safebits\Connector\Connector;

use Safebits\Connector\Exceptions\MissingConfigurationFileExceptionSB;

/**
 * Class SBExternalConnector
 * @package Safebits\Connector
 */
class SBExternalConnector extends SBConnector
{
    /**
     * @var
     */
    private $key;

    /**
     * @var
     */
    private $secret;

    /**
     * @var
     */
    private $nonce;

    /**
     * SBExternalConnector constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->checkConfigFile();

        $this->url = config('safebits_connector.url');
        $this->timeout = config('safebits_connector.timeout');
        $this->connectTimeout = config('safebits_connector.connect_timeout');
        $this->readTimeout = config('safebits_connector.read_timeout');
    }

    /**
     * @param $endpoint
     * @param $params
     * @param $customHeaders
     * @return array
     * @throws \Exception
     */
    protected function getHeaders($endpoint, $params, $customHeaders)
    {
        // If the wallet consumer does not provide the necessary credentials, then, the request will use the ones defined in the config file
        $this->checkRequestInfo();

        // Sets the headers for the request
        $requestHeaders = [];
        $requestHeaders['Accept'] = 'application/json';
        $requestHeaders['X-Crypto-Key'] = $this->key;
        $requestHeaders['X-Crypto-Nonce'] = $this->nonce;
        $requestHeaders['X-Crypto-Signature'] = $this->getRequestSignature($endpoint, $params, $this->secret, $this->nonce);

        $finalHeaders = array_merge($requestHeaders, $customHeaders);

        return $finalHeaders;
    }

    /**
     * @throws \Exception
     */
    private function checkConfigFile()
    {
        if (!config('safebits_connector')) {
            throw new MissingConfigurationFileExceptionSB();
        }
    }

    /**
     * @param $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @param $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @param $nonce
     */
    public function setNonce($nonce)
    {
        $this->nonce = $nonce;
    }

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     *
     */
    private function checkRequestInfo()
    {
        if (!$this->key) {
            $this->key = config('safebits_connector.key');
        }

        if (!$this->secret) {
            $this->secret = config('safebits_connector.secret');
        }

        if (!$this->nonce) {
            $this->nonce = time();
        }

        if (!$this->url) {
            $this->url = config('safebits_connector.url');
        }
    }
}
