<?php

namespace Safebits\Connector\Connector;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use Safebits\Connector\Exceptions\ApiRequestException;
use Safebits\Connector\Exceptions\BadResponseExceptionSB;
use Safebits\Connector\Exceptions\InvalidRequestEncodingExceptionSB;
use Safebits\Connector\Models\SBConnectorErrorResponse;
use Safebits\Connector\Models\SBConnectorSuccessResponse;

/**
 * Class Connector
 * @package Safebits\Connector
 */
abstract class SBConnector
{
    const ENCODING = "UTF-8";

    const HASH_ALGORITHM_SHA256 = "sha256";

    const HASH_ALGORITHM_SHA512 = "sha512";

    /**
     * @var $url
     */
    protected $url;

    /**
     * @var $timeout
     */
    protected $timeout;

    /**
     * @var $connectTimeout
     */
    protected $connectTimeout;

    /**
     * @var $readTimeout
     */
    protected $readTimeout;

    /**
     * @var $requestMethod
     */
    protected $requestMethod;

    /**
     * @param $endpoint
     * @param array $params
     * @param array $customHeaders
     * @return SBConnectorErrorResponse|SBConnectorSuccessResponse
     * @throws ApiRequestException
     * @throws BadResponseExceptionSB
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($endpoint, $params = [], $customHeaders = [])
    {
        $this->requestMethod = 'POST';

        $url = $this->url . $endpoint;

        $headers = $this->getHeaders($endpoint, $params, $customHeaders);

        $request = new Request('POST', $url);

        $client = new Client();

        $options = ['form_params' => $params, 'headers' => $headers, 'timeout' => $this->timeout, 'connect_timeout' => $this->connectTimeout, 'read_timeout' => $this->readTimeout];

        try {
            $response = $client->send($request, $options);
            $responseContent = $response->getBody()->getContents();
            $HTTPStatusCode = $response->getStatusCode();
            $HTTPHeaders = $response->getHeaders();
        } catch (ClientException $exception) {
            $responseContent = $exception->getResponse()->getBody()->getContents();
            $HTTPStatusCode = $exception->getResponse()->getStatusCode();
            $HTTPHeaders = $exception->getResponse()->getHeaders();
        } catch (\Exception $exception) {
            throw new ApiRequestException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $this->checkApiResponse(json_decode($responseContent), $HTTPStatusCode, $HTTPHeaders);
    }

    /**
     * @param $endpoint
     * @param array $params
     * @param array $customHeaders
     * @return SBConnectorErrorResponse|SBConnectorSuccessResponse
     * @throws ApiRequestException
     * @throws BadResponseExceptionSB
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($endpoint, $params = [], $customHeaders = [])
    {
        $this->requestMethod = 'GET';

        $url = $this->url . $endpoint;

        $headers = $this->getHeaders($endpoint, $params, $customHeaders);
        $request = new Request('GET', $url);

        $client = new Client();

        $options = ['query' => $params, 'headers' => $headers, 'timeout' => $this->timeout, 'connect_timeout' => $this->connectTimeout, 'read_timeout' => $this->readTimeout];

        try {
            $response = $client->send($request, $options);
            $responseContent = $response->getBody()->getContents();
            $HTTPStatusCode = $response->getStatusCode();
            $HTTPHeaders = $response->getHeaders();
        } catch (ClientException $exception) {
            $responseContent = $exception->getResponse()->getBody()->getContents();
            $HTTPStatusCode = $exception->getResponse()->getStatusCode();
            $HTTPHeaders = $exception->getResponse()->getHeaders();
        } catch (\Exception $exception) {
            throw new ApiRequestException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $this->checkApiResponse(json_decode($responseContent), $HTTPStatusCode, $HTTPHeaders);
    }

    /**
     * @param $endpoint
     * @param $params
     * @param $customHeaders
     * @return mixed
     */
    protected abstract function getHeaders($endpoint, $params, $customHeaders);

    /**
     * @param $endpoint
     * @param $params
     * @param $secret
     * @param $nonce
     * @return string
     * @throws InvalidRequestEncodingExceptionSB
     */
    protected function getRequestSignature($endpoint, $params, $secret, $nonce)
    {
        //When creating the signature, we need just the string that comes after /v1
        $startIndex = strpos($endpoint, "/v1");
        if (!$startIndex) {
            $startIndex = strpos($endpoint, "/internal");
        }

        $endpoint = substr($endpoint, $startIndex);

        if ($this->requestMethod == 'POST') {
            $requestData = json_encode($params);
        } else {
            if (is_array($params)) {
                $requestData = json_encode($params);
            } else {
                $requestData = http_build_query($params);
            }
        }

        $encoding = mb_detect_encoding($requestData, self::ENCODING);
        if ($encoding != self::ENCODING) {
            throw new InvalidRequestEncodingExceptionSB();
        }
        $requestData = strtolower(hash(self::HASH_ALGORITHM_SHA256, $requestData, false));
        $msg = $endpoint . $nonce . $requestData;
        $signature = hash_hmac(self::HASH_ALGORITHM_SHA512, $msg, $secret);

        return $signature;
    }

    /**
     * @param $responseContent
     * @param $HTTPStatusCode
     * @param $HTTPHeaders
     * @return SBConnectorErrorResponse|SBConnectorSuccessResponse
     * @throws BadResponseExceptionSB
     */
    protected function checkApiResponse($responseContent, $HTTPStatusCode, $HTTPHeaders)
    {
        if ($responseContent && property_exists($responseContent, "status")) {

            $status = $responseContent->status;
            $message = $responseContent->message;

            if ($status) {
                $payload = property_exists($responseContent, "payload") ? $responseContent->payload : null;
                $response = new SBConnectorSuccessResponse($message, $payload);
            } else {
                $reasonCode = $responseContent->reasonCode;
                $reason = $responseContent->reason;
                $response = new SBConnectorErrorResponse($message, $reasonCode, $reason);
            }

            $response->HTTPStatusCode = $HTTPStatusCode;
            $response->HTTPHeaders = $HTTPHeaders;

            return $response;
        } else {
            throw new BadResponseExceptionSB();
        }
    }
}
