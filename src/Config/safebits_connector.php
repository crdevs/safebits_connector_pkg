<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Credentials
    |--------------------------------------------------------------------------
    |
    | Here you may specify the location of the credentials to access the
    | different services
    |
    */

    'key' => env('SB_CLIENT_DIVISION_KEY'),
    'secret' => env('SB_CLIENT_DIVISION_SECRET'),

    /*
    |--------------------------------------------------------------------------
    | Connection Configurations
    |--------------------------------------------------------------------------
    |
    | Here you may specify the fields that are necessary for the connection
    | between external systems to the gateway system
    |
    */
    'url' => env('SB_GATEWAY_URL'),

    /*
    |--------------------------------------------------------------------------
    | Request Configurations
    |--------------------------------------------------------------------------
    |
    | Here you may specify the fields that are necessary for the different
    | requests that the package will perform
    |
    */
    'timeout' => env('REQUEST_TIMEOUT', 20),
    'connect_timeout' => env('REQUEST_CONNECT_TIMEOUT', 10),
    'read_timeout' => env('REQUEST_READ_TIMEOUT', 10),

];