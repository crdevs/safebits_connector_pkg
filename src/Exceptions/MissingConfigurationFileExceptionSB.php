<?php

namespace Safebits\Connector\Exceptions;

use Safebits\Connector\Constants\ConnectorConstants;

/**
 * Class MissingConfigurationFileException
 * @package Safebits\Connector
 */
class MissingConfigurationFileExceptionSB extends SBConnectorException
{
    /**
     * MissingConfigurationFileException constructor.
     */
    public function __construct()
    {
        $message = ConnectorConstants::MISSING_CONFIGURATION_FILE_EXCEPTION_MESSAGE;
        $code = ConnectorConstants::MISSING_CONFIGURATION_FILE_EXCEPTION_CODE;

        parent::__construct($message, $code);
    }
}
