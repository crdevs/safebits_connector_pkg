<?php

namespace Safebits\Connector\Models;

/**
 * Class SBConnectorResponse
 * @package Safebits\Connector
 */
class SBConnectorResponse
{
    /**
     * @var int $HTTPStatusCode
     */
    public $HTTPStatusCode;

    /**
     * @var array $HTTPHeaders
     */
    public $HTTPHeaders;

    /**
     * @var int $status
     */
    public $status;

    /**
     * @var $message
     */
    public $message;

    /**
     * SBConnectorResponse constructor.
     * @param $status
     * @param $message
     */
    public function __construct($status, $message)
    {
        $this->status = $status;
        $this->message = $message;
    }
}
