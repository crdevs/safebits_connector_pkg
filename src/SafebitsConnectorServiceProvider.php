<?php

namespace Safebits\Connector;

use Illuminate\Support\ServiceProvider;

/**
 * Class SafebitsConnectorServiceProvider
 * @package Safebits\Connector
 */
class SafebitsConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_connector.php' => config_path('safebits_connector.php'),
        ]);
    }
}
