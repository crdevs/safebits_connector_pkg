<?php

namespace Safebits\Connector\Exceptions;

/**
 * Class ApiRequestException
 * @package Safebits\Connector
 */
class ApiRequestException extends SBConnectorException
{
    /**
     * ApiRequestException constructor.
     * @param $message
     * @param $code
     * @param $previous
     */
    public function __construct($message, $code, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
