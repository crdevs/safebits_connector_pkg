<?php

namespace Safebits\Connector\Constants;

/**
 * Class ConnectorConstants
 * @package Safebits\Connector
 */
abstract class ConnectorConstants
{
    /**
     *
     */
    const BAD_RESPONSE_EXCEPTION_MESSAGE = "The service returned an invalid response";

    /**
     *
     */
    const BAD_RESPONSE_EXCEPTION_CODE = 502;

    /**
     *
     */
    const MISSING_CONFIGURATION_FILE_EXCEPTION_MESSAGE = "Connector configuration file does not exist. Run 'php artisan vendor:publish'";

    /**
     *
     */
    const MISSING_CONFIGURATION_FILE_EXCEPTION_CODE = 1;

    /**
     *
     */
    const INVALID_REQUEST_ENCODING_EXCEPTION_MESSAGE = "Invalid Request Encoding Exception";

    /**
     *
     */
    const INVALID_REQUEST_ENCODING_EXCEPTION_CODE = 2;
}
