<?php

namespace Safebits\Connector\Exceptions;

use Safebits\Connector\Constants\ConnectorConstants;

/**
 * Class InvalidRequestEncodingException
 * @package Safebits\Connector
 */
class InvalidRequestEncodingExceptionSB extends SBConnectorException
{
    /**
     * InvalidRequestEncodingException constructor.
     */
    public function __construct()
    {
        $message = ConnectorConstants::INVALID_REQUEST_ENCODING_EXCEPTION_MESSAGE;
        $code = ConnectorConstants::INVALID_REQUEST_ENCODING_EXCEPTION_CODE;

        parent::__construct($message, $code);
    }
}
