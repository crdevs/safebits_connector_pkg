<?php

namespace Safebits\Connector\Models;

/**
 * Class SBConnectorSuccessResponse
 * @package Safebits\Connector
 */
class SBConnectorSuccessResponse extends SBConnectorResponse
{
    /**
     * @var $payload
     */
    public $payload;

    /**
     * ConnectorSuccessResponse constructor.
     * @param $message
     * @param $payload
     */
    public function __construct($message, $payload)
    {
        parent::__construct(1, $message);
        $this->payload = $payload;
    }
}
