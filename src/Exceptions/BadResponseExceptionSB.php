<?php

namespace Safebits\Connector\Exceptions;

use Safebits\Connector\Constants\ConnectorConstants;

/**
 * Class BadResponse
 * @package Safebits\Connector
 */
class BadResponseExceptionSB extends SBConnectorException
{
    /**
     * BadResponse constructor.
     */
    public function __construct()
    {
        $message = ConnectorConstants::BAD_RESPONSE_EXCEPTION_MESSAGE;
        $code = ConnectorConstants::BAD_RESPONSE_EXCEPTION_CODE;

        parent::__construct($message, $code);
    }
}
