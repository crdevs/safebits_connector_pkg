<?php

namespace Safebits\Connector\Models;

/**
 * Class SBConnectorErrorResponse
 * @package Safebits\Connector
 */
class SBConnectorErrorResponse extends SBConnectorResponse
{
    /**
     * @var $reasonCode
     */
    public $reasonCode;

    /**
     * @var $reason
     */
    public $reason;

    /**
     * ConnectorErrorResponse constructor.
     * @param $message
     * @param $reasonCode
     * @param $reason
     */
    public function __construct($message, $reasonCode, $reason)
    {
        parent::__construct(0, $message);
        $this->reasonCode = $reasonCode;
        $this->reason = $reason;
    }
}
